/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_ex1;

/**
 *
 * @author Renan
 */
public class Animal extends SerVivo {
    private boolean Locomover;
    public Animal(boolean Locomover, boolean Vida){
        super(Vida);
        this.Locomover = Locomover;
    }
    public boolean isLocomover() {
        return Locomover;
    }

    public void setLocomover(boolean Locomover) {
        this.Locomover = Locomover;
    }

}
