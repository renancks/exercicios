/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_ex1;

/**
 *
 * @author Renan
 */
public class AnimalInvertebrado extends Animal {
    private boolean exoEsqueleto;

    public AnimalInvertebrado(boolean exoEsqueleto,boolean Locomover, boolean isVida) {
        super(Locomover, isVida);
        this.exoEsqueleto = exoEsqueleto; 
    }

    public boolean isExoEsqueleto() {
        return exoEsqueleto;
    }

    public void setExoEsqueleto(boolean exoEsqueleto) {
        this.exoEsqueleto = exoEsqueleto;
    }
    
}
