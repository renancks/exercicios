/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_ex1;

/**
 *
 * @author Renan
 */
public class AnimalVertebrado extends Animal{
    boolean Vertebras;
    public AnimalVertebrado(boolean Vertebras, boolean Locomover, boolean Vida){
    super(Locomover, Vida);
    this.Vertebras = Vertebras;
    }

    public boolean isVertebras() {
        return Vertebras;
    }

    public void setVertebras(boolean Vertebras) {
        this.Vertebras = Vertebras;
    }
    
}
