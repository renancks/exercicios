/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_ex1;

/**
 *
 * @author Renan
 */
public class SerVivo {
    boolean Vida;

    public SerVivo(boolean Vida) {
        this.Vida = Vida;
    }

    public boolean isVida() {
        return Vida;
    }

    public void setVida(boolean Vida) {
        this.Vida = Vida;
    }
    
    public void Alimentar(){
        Vida = true; 
    }
    public void Respirar(){
        Vida = true;
    }
    public void Reproduzir(){
        Vida = true;
    }
    public void Morrer(){
        Vida = false;
    }
    
}
